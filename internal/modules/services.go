package modules

import (
	"gitlab.com/kuzmem/goboilerplate/internal/infrastructure/component"
	aservice "gitlab.com/kuzmem/goboilerplate/internal/modules/auth/service"
	uservice "gitlab.com/kuzmem/goboilerplate/internal/modules/user/service"
	"gitlab.com/kuzmem/goboilerplate/internal/storages"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
